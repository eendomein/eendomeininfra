include {
  path = find_in_parent_folders()
}

locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

terraform {
  source = "tfr:///terraform-aws-modules/acm/aws?version=5.0.1"
}

dependency "route53-eendomein-zone" {
  config_path = "../../eu-north-1/eendomeinnl/r53-eendomein"
  mock_outputs = {
    route53_zone_zone_id = {
      "${local.common_vars.www.domain}" = "zone-id-34234234"
    }
  }
}

inputs = {
  domain_name  = local.common_vars.www.domain
  zone_id      = dependency.route53-eendomein-zone.outputs.route53_zone_zone_id[local.common_vars.www.domain]

  subject_alternative_names = local.common_vars.www.aliases

  validation_method = "DNS"
  wait_for_validation = true

  tags = {
    ManagedBy = "Gitops"
  }
}
