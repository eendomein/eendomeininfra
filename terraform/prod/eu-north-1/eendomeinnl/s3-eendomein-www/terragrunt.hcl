include {
  path = find_in_parent_folders()
}

locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

terraform {
  source = "tfr:///terraform-aws-modules/s3-bucket/aws?version=4.1.1"
}

inputs = {
  bucket = local.common_vars.www.bucket
  tags = {
    Owner = "eendomeinnl-www"
  }
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false

  website = {
    index_document = "index.html"
  }

  attach_policy = true
  policy = templatefile("s3-access-policy.tpl", { gitops_arn = local.common_vars.gitops_arn, bucket = local.common_vars.www.bucket  })
}
