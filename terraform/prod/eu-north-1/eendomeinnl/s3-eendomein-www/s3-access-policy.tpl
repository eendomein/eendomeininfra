
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "GitopsAccess",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${gitops_arn}"
            },
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::${bucket}",
                "arn:aws:s3:::${bucket}/*"
            ]
        },
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::${bucket}/*"
            ]
        }
    ]
}
