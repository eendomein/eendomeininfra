include {
  path = find_in_parent_folders()
}

locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

prevent_destroy = true

terraform {
  source = "tfr:///terraform-aws-modules/route53/aws//modules/records?version=2.11.1"
}

dependency "cf-eendomein" {
  config_path = "../cf-eendomein"
  mock_outputs = {
    cloudfront_distribution_domain_name = "${local.common_vars.www.domain}.cloudfront.dummy"
    cloudfront_distribution_hosted_zone_id = "${local.common_vars.www.domain}-id.cloudfront.dummy"
  }
}

inputs = {
  zone_name = local.common_vars.www.domain

  tags = {
    ManagedBy = "Gitops"
  }

  records = [
    {
      name = ""
      type = "A"
      alias = {
        name = dependency.cf-eendomein.outputs.cloudfront_distribution_domain_name
        zone_id = dependency.cf-eendomein.outputs.cloudfront_distribution_hosted_zone_id
      }
    },
    {
      name = "www"
      type = "A"
      alias = {
        name = dependency.cf-eendomein.outputs.cloudfront_distribution_domain_name
        zone_id = dependency.cf-eendomein.outputs.cloudfront_distribution_hosted_zone_id
      }
    }
  ]
}

