include {
  path = find_in_parent_folders()
}

locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

terraform {
  source = "tfr:///terraform-aws-modules/cloudfront/aws?version=3.4.0"
}

dependency "acm-eendomein-nl" {
  config_path = "../../../us-east-1/acm-eendomein-nl"
  mock_outputs = {
    acm_certificate_arn = "arn:aws:acm:eu-north-1:444455556666:certificate/bogus_dummy_id"
  }
}

dependency "s3-eendomein-www" {
  config_path = "../s3-eendomein-www"
  mock_outputs = {
    s3_bucket_website_endpoint = "eendomein.nl.s3-website.eu-north-1.amazonaws.com"
  }
}

inputs = {
  aliases = [ local.common_vars.www.domain ]

  comment             = "Cloudfront acceleration for ${local.common_vars.www.domain}"
  enable              = true
  is_ipv6_enabled     = true
  retain_on_delete    = false
  wait_for_deployment = false
  default_root_object = "index.html"

  origin = {
    s3_blokje_in = {
      domain_name = dependency.s3-eendomein-www.outputs.s3_bucket_website_endpoint
      custom_origin_config = {
        http_port              = "80"
        https_port             = "443"
        origin_protocol_policy = "http-only"
        origin_read_timeout    = "60"
        origin_ssl_protocols   = ["TLSv1.2", "TLSv1.1", "TLSv1"]
      }
    }
  }

  default_cache_behavior = {
    target_origin_id           = "s3_blokje_in"
    viewer_protocol_policy     = "redirect-to-https"

    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods  = ["GET", "HEAD"]
    compress        = true
    query_string    = true

    min_ttl                = 0
    default_ttl            = 300
    max_ttl                = 3600
  }

  viewer_certificate = {
    acm_certificate_arn = dependency.acm-eendomein-nl.outputs.acm_certificate_arn
    ssl_support_method  = "sni-only"
  }
}
