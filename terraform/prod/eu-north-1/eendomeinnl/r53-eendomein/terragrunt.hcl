include {
  path = find_in_parent_folders()
}

prevent_destroy = true

locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

terraform {
  source = "tfr:///terraform-aws-modules/route53/aws//modules/zones?version=2.11.1"
}


inputs = {
  create = true
  zones = {
    "${local.common_vars.www.domain}" = {
      comment = "${local.common_vars.www.domain} domain"
      tags = {
        ManagedBy = "Gitops"
      }
    }
  }

  tags = {
    ManagedBy = "Gitops"
  }
}

